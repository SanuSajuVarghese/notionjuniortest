<?php

namespace App\Http\Controllers;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    
    public function index(){
        $product=Product::all();
        
        return response()->json($product); 

    
    }
    public function add(Request $request){
        $product=Product::Create([
            'SKU'=>$request->SKU,
            'Name'=>$request->Name,
            'Price'=>$request->Price,
            'ProductType'=>$request->ProductType,
            'Size'=>$request->Size

        ]);
        return response()->Json($product);
    }
    public function delete(Request $request){
        
        Product::whereIn('id',$request->id)->delete();
        return response()->json(['success']);
    }
    public function isUserNameInUse( Request $request)
    {
        info('This is some useful information.');
          $num=Product::where('SKU',  $request->sku)->count();
          info($num);
        if ($num>0 ){
            return response()->json([ 'is_used' ]);
        }

            return response()->json([ 'not'  ]);
    }
}
