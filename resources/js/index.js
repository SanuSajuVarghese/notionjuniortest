import React from 'react';
import ReactDOM from 'react-dom';
import Home from './components/Home';
import Create from './components/Create';
import { BrowserRouter as  Router , Switch, Route } from 'react-router-dom';


function Index (){
  console.info("comming");
    return (
      <Router>
      <Switch>
          <Route path="/" exact component={Home}/>
          <Route path="/addproduct" exact component={Create}/>
      </Switch>
      </Router>
    );
}

export default Index;
if (document.getElementById('app')) {
    ReactDOM.render(<Index />, document.getElementById('app'));
}