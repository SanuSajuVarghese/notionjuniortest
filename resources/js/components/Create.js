import axios from 'axios';
import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import {userForm} from 'react-hook-form';

class Create extends Component {
    
  
    state={
        SKU:"",
        Name:"",
         Price:"",
         ProductType:"",
         Size:"",
         height:"",
         length:"",
         width:"",
         weight:""
   }

    componentDidMount() {
        this.myFunction()
        this.validateselect()
    }
    onSubmit=(e)=>{
        e.preventDefault();
        const isValid=this.validate();
        if(isValid){
            var si="";
            if(this.state.ProductType=="Furniture"){
                  si='Dimension: ' +this.state.height +'×'+this.state.length+'×'+this.state.width ;
            }
            else if(this.state.ProductType=="Book"){
              si='Weight: '+this.state.weight +' KG';
            }
            else{
                si='Size: ' +this.state.Size +' MB';
            }
            var pri=this.state.Price+ ' $';
            console.log(si);
          axios.post("/api/add",{
              SKU:this.state.SKU,
              Name:this.state.Name,
              Price:pri,
              ProductType:this.state.ProductType,
              Size:si
          })  .then(()=>{window.location.href = '/';})
          .catch(function(error) {
              console.log(error);
          })
          }
        
       

  }

        
  onchange=(e)=>{
    this.setState({[e.target.name]:e.target.value});
   
    if(e.target.name=="ProductType"){
       this. validateselect();
    }
  
  }

  
  onkeyup=(e)=>{
    console.log("keyup");
    axios.post("/api/validsku",{
        sku:e.target.value
     }) .then(response=>{
        if(response.data=='is_used'){
            console.log(response.data);
            document.getElementById("wrapper").innerHTML ="  SKU already exist";
        }
        else{
            document.getElementById("wrapper").innerHTML ="";
        }
     })
}
    

    myFunction(){          
         
        $(".details").hide();
       $(".p").hide();
    }

  
  validate() {
           
        var e = document.getElementById("productType"),
            t = document.getElementById("wrapper").innerHTML,
            n = (document.getElementById("name"), document.getElementById("price")),
            a = document.getElementById("size"),
            l = document.getElementById("length"),
            s = document.getElementById("width"),
            c = document.getElementById("height"),
            m = document.getElementById("weight"),
            u = (n.value, a.value),
            o = m.value,
            r = l.value,
            d = c.value,
            i = s.value;
        if ((console.log(t), "" != t)) {
            var g = document.getElementById("snackbar");
            return (
                (document.getElementById("ar").innerHTML = "sku is already exist"),
                (g.className = "show"),
                setTimeout(function () {
                    g.className = g.className.replace("show", "");
                }, 3e3),
                 false
            );
        }
        if ("Type Switcher" == e.value) {
            g = document.getElementById("snackbar");
            return (
                (document.getElementById("ar").innerHTML = "select product type"),
                (g.className = "show"),
                setTimeout(function () {
                    g.className = g.className.replace("show", "");
                }, 3e3),
                false
            );
        }
        if ("DVD" == e.value && "" == u) {
            g = document.getElementById("snackbar");
            return (
                (document.getElementById("ar").innerHTML = "please provide size "),
                (g.className = "show"),
                setTimeout(function () {
                    g.className = g.className.replace("show", "");
                }, 3e3),
                false
            );
        }
        if ("Furniture" == e.value && ("" == r || "" == d || "" == i)) {
            g = document.getElementById("snackbar");
            return (
                (document.getElementById("ar").innerHTML = "please fill all columns"),
                (g.className = "show"),
                setTimeout(function () {
                    g.className = g.className.replace("show", "");
                }, 3e3),
                false
            );
        }
        if ("Book" == e.value && "" == o) {
            g = document.getElementById("snackbar");
            return (
                (document.getElementById("ar").innerHTML = "provide weight "),
                (g.className = "show"),
                setTimeout(function () {
                    g.className = g.className.replace("show", "");
                }, 3e3),
                false
            );
        }
        return true;
    }
   
      
 

  validateselect(){
      
      var e=document.getElementById("productType")
      var type=e.value;         
        $(".details").hide();
        $(".p").hide();
         $("#" + type).show();
         $("#"+ type +"p").show();
      
        
    }

    render(){

        return (
            <div className="container">
                                

                <div class="container" id="header">
                <div class="row">
                <div class="pull-left">
                <h1>Product Add</h1>
                </div>
                <div class="pull-right">
                
                <form   id= "product_form" onSubmit={this.onSubmit}>
                
                <input type="submit" value="Save" form="product_form" id ="save" />

                </form>
                </div>
                   
                    
                    <div class="pull-right">
        <form  id="cancel" action="/"></form>

    <button type="submit" value="cancel" 
    id="delete-product-btn" name="delete-product-btn"  form="cancel"  >Cancel</button>

                </div>
                </div>

                </div>

                <div class="container" id ="second" onLoad={this.myFunction} >
                
                <table class="tabl" >
                    <tr> <td>SKU</td> 
                        <td><input type="text" value={this.state.SKU} name="SKU" id="sku" form="product_form" onKeyUp={this.onkeyup} onChange ={this.onchange}required/>
                        </td>
                    
                        <td>Name</td>
                        <td><input type="text" value={this.state.Name} name="Name" id="name" form="product_form" onChange ={this.onchange}required/> </td>
                    </tr>
                    <tr><td><p id="wrapper"></p></td></tr>
                    <tr><td><span id="status"></span></td></tr>
                    <tr> <td>Price($)</td> 
                        <td><input type="number" value={this.state.Price} name="Price" id="price" form="product_form" onChange ={this.onchange} required/>
                        </td>
                        <td class="ADD">Type switcher </td>
                        <td>
                                        <select id="productType" name="ProductType" value={this.state.ProductType} form="product_form" onChange ={this.onchange} required>
                                        <option  value="Type Switcher">Type Switcher</option>
                                        <option value="DVD" id="DVD1">DVD</option>
                                        <option value="Furniture" id="Furniture1">Furniture</option>
                                        <option value="Book" id="Book1">Book</option>
                                    
                                        </select>
                            </td>
                    </tr>
                    <tr id="DVD" class="details" >
                        <td  >Size</td>
                        <td><input type="text" value={this.state.Size} name="Size" id="size" form="product_form" onChange ={this.onchange}/></td>
                        
                    </tr>
                    <tr id="DVDp" class="p"><td></td><td> <p >Please, provide size in MB</p></td></tr>
                    <tr id="Furniture" class="details">
                        <td  >Dimensions</td>
                            <td><input type="number" value={this.state.height} name="height" id="height" placeholder ="height"form="product_form" onChange ={this.onchange}/></td>
                            <td><input type="number" value={this.state.width}name="width" id="width" placeholder="width"form="product_form" onChange ={this.onchange}/></td>
                            <td><input type="number" value={this.state.length}name="length" id="length" placeholder="length"form="product_form" onChange ={this.onchange}/></td>
                            
                    </tr>
                    <tr id="Furniturep" class="p"><td></td> <td><p >Please, provide dimensions</p></td></tr>
                    <tr id="Book" class="details"  >
                        <td  >weight</td>
                        <td><input type="number"  value={this.state.weight} name="weight" id="weight"form="product_form" onChange ={this.onchange}/></td>
                    
                    </tr>
                    <tr id="Bookp" class="p"> <td></td><td> <p >Please, provide weight in KG</p></td></tr>
                        
                    </table>
                    <div id="snackbar"><p id="ar">So</p></div>
                </div>

                <div class="container" id="foot">
                <footer>Scandiweb Test assignment</footer>
                </div>
                            </div>
                        );
                    }
            }

export default Create;