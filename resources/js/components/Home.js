import axios from 'axios';
import React,{Component,useState,useEffect} from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router-dom'
import {userForm} from 'react-hook-form';
import { ids } from 'webpack';


class  Home extends Component{
    constructor(props){
        super(props);
        
    }
          

    state={products:[],
        itemschecked:[]
        }
       
    
    onChange(e){
        let itemchecked=this.state.itemschecked;
        if(e.target.checked){
       this.state.itemschecked.push(e.target.value);
        }
        else{
            var index=this.state.itemschecked.indexOf(e.target.value);
            this.state.itemschecked.splice(index,1 );
            
        }
       
        console.log(itemchecked);
        this.setState(itemchecked);
  }  
  
  validatedelete(ids){
    if(ids.length==0){
        var g = document.getElementById("snackbar");
        return (
            (document.getElementById("ar").innerHTML = "sku is already exist"),
            (g.className = "show"),
            setTimeout(function () {
                g.className = g.className.replace("show", "");
            }, 3e3),
             false
        );
    }
    else{
        return true;
    }
  }
 
        delete=(e)=>{
       e.preventDefault();

        let ids=[];
        this.state.itemschecked.map(d=>{
            ids.push(d);
        })
        const isValid=this.validatedelete(ids);
       
        if(isValid){
        axios.post("/api/delete",{
            id:ids
            
        })  .then(()=>{window.location.href = '/';})
        .catch(function(error) {
            console.log(error);
        })
         }
            
            
        }
 
    

        componentDidMount(){
            
            axios.post('/api/products')
            .then(response=>{
                this.setState({
                    products:response.data
                }).catch(err => console.log(err));
            })
            this.onChange=this.onChange.bind(this);
        }


    render(){
        
        return (
            <div className="container">
                <div class="container" id="header">
    <div class="row">
    <div class="pull-left">
    <h1>Product List</h1>
    </div>
    <div class="pull-right">
    
    <form action="/addproduct" class="add">
    
        <button type="submit" value="ADD">ADD</button>
        </form>
    </div>
        <div class="pull-right">
        <form  id="deletemass" onSubmit={this.delete}></form>

    <button type="submit" value="MASS DELETE" 
    id="delete-product-btn" name="delete-product-btn"  form="deletemass"  >MASS DELETE</button>


    </div>
    </div>

    </div>

    <div class ="container" id="secondcontainer"> 
        <div class="row">
                
        
            {      this.state.products !== null ?
                        this.state.products.map(product=>(
                            <div class="col-md-3">
    <div class="thumbnail">
                        
                        <input type='checkbox'name="delete-checkbox[]" id="delete-checkbox" class="delete-checkbox" value={product.id} form="deletemass" onChange={this.onChange}/>
               
                            <div class="caption">
                            <h6>{product.Name}</h6>
                            <h6>{product.SKU}</h6>
                            <h6>{product.Price}</h6>
                            <h6>{product.Size}</h6>
                               
                            </div>
                            </div>
                            </div>
                        )):null

                    
            }
            
            
            <div id="snackbar"><p id="ar">So</p></div>

    </div>
    </div>
   
    <div class="container" id="foot">
    <footer>Scandiweb Test assignment</footer>
    </div>
            </div>
        );
        }
    }

export default Home;

if (document.getElementById('app')) {
    ReactDOM.render(<Home />, document.getElementById('app'));
}
